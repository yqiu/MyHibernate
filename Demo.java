package test;

import java.util.ArrayList;
import java.util.List;

import org.myhibernate.core.Template;
import org.myhibernate.core.method.PageSizeQuery;
import org.myhibernate.entity.UserEntity;
/**
 * 该框架对批量进行操作时，使用的是扩展sql
 * 扩展sql执行的api对象是 PageSizeQuery
 * 扩展sql：是指sql的组成部分只从 where条件开始,具体事例参照deleteBatchObject和getBatchObject
 * 该Demo中的操作实体对象UserEntity，与数据表的映射关系是默认映射方式
 * 默认映射方式：无需配置src目录下的application.xml文件，但是要满足以下要求：
 * 	1、实体名和数据表名一致，实体属性名和数据表字段列名一致
 *  2、实体中一定要有一个属性id来作为唯一标识
 *  
 * @author liujun
 * @chinese 刘军
 * @date 2015-04-17
 */
public class Demo
{
	/**
	 * 保存一个实体对象
	 */
	public void saveOneObject()
	{
		UserEntity user=null;
		user=new UserEntity();
		user.setName("张三");
		user.setAge(24);
		user.setSex("男性");
		
		Template<UserEntity> template=new Template<UserEntity>(UserEntity.class);
		try {
			template.save(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		template.close();
	}
	/**
	 * 保存一批实体对象
	 */
	public  void saveBatchObject() 
	{
		List<UserEntity> list=new ArrayList<UserEntity>();
		UserEntity user=null;
		user=new UserEntity();
		user.setName("张三");
		user.setAge(24);
		user.setSex("男性");
		list.add(user);
		
		user=new UserEntity();
		user.setName("李四");
		user.setAge(25);
		user.setSex("男性");
		list.add(user);
		
		user=new UserEntity();
		user.setName("苏敏");
		user.setAge(22);
		user.setSex("女性");
		list.add(user);
		
		Template<UserEntity> template=new Template<UserEntity>(UserEntity.class);
		try {
			template.save(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		template.close();
		
		
		System.out.println("finish");
	}
	/**
	 * 通过id，删除一个实体对象
	 */
	public void deleteOneObject()
	{
		String id="zhangsan";
		
		Template<UserEntity> template=new Template<UserEntity>(UserEntity.class);
		try {
			template.delete(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		template.close();
	}
	/**
	 * 通过扩展sql，删除一批实体对象
	 */
	public void deleteBatchObject()
	{
		String extSql=" where age=24 ";
		
		Template<UserEntity> template=new Template<UserEntity>(UserEntity.class);
		PageSizeQuery<UserEntity> query=(PageSizeQuery<UserEntity>) template.getPageSizeQuery();
		try {
			query.deletes(extSql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		template.close();
	}
	/**
	 * 更新一个实体对象
	 */
	public void update()
	{
		UserEntity user=null;
		user=new UserEntity();
		user.setId("675a9e05-a465-4d5b-8d12-9635ef153ea5");
		user.setName("张三");
		user.setAge(24);
		user.setSex("男性");
		
		Template<UserEntity> template=new Template<UserEntity>(UserEntity.class);
		try {
			template.update(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		//template.clear(); //该方法可选，如果在更新对象的时候，需要更新sql 语句缓存的，则可以执行该方法
		template.close();
	}
	/**
	 * 只更新实体对象中指定的某一个或者几个属性
	 * 这里只更新 属性age和name的值到数据库
	 */
	public void updateField()
	{
		UserEntity user=null;
		user=new UserEntity();
		user.setId("675a9e05-a465-4d5b-8d12-9635ef153ea5");
		user.setName("张三");
		user.setAge(24);
		user.setSex("男性");
		
		Template<UserEntity> template=new Template<UserEntity>(UserEntity.class);
		try {
			template.update(user,new String[]{"age","name"});
		} catch (Exception e) {
			e.printStackTrace();
		}
		//template.clear(); //该方法可选，如果在更新对象的时候，需要更新sql 语句缓存的，则可以执行该方法
		template.close();
	}
	/**
	 * 通过id获取一个实体对象
	 */
	public void getObjectById()
	{
		String id="675a9e05-a465-4d5b-8d12-9635ef153ea5";
		UserEntity user=null;
		Template<UserEntity> template=new Template<UserEntity>(UserEntity.class);
		try {
			user=template.get(id);
			System.out.println("user="+user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		template.close();
	}
	/**
	 * 通过扩展sql
	 * 分页的获取一批实体对象
	 */
	public void getBatchObject()
	{
		List<UserEntity> list=null;
		int start=0,size=10,count=0;
		String extSql=" where age=24 ";
		Template<UserEntity> template=new Template<UserEntity>(UserEntity.class);
		PageSizeQuery<UserEntity> query=(PageSizeQuery<UserEntity>) template.getPageSizeQuery();
		try {
			count=query.query(extSql);
			list=query.getListObject(start,start+size);
			System.out.println("list="+list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		template.close();
	}
	public static void main(String[] args) {
		Demo demo=new Demo();
		//demo.saveOneObject();
		demo.getBatchObject();
		System.out.println("finsih");
	}
}
