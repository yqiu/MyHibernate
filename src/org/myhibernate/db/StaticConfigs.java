package org.myhibernate.db;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
public class StaticConfigs 
{
	private static Properties properties=null;
	private static String path="org/myhibernate/db/dbconfig.properties";
	static
	{
		properties=new Properties();
		try {
			reload();
		} catch (Exception e) 
		{ 
			//System.out.println("StaticConfig static reload 1 exception..."+new File(path).getAbsolutePath());
			e.printStackTrace();
		}
	}
	public static String getAttribute(String key)
	{
		return properties.getProperty(key); 
	}
	public static void reload()
	{
		try {
			properties.load(StaticConfigs.class.getClassLoader().getResourceAsStream("dbconfig.properties"));
			System.out.println("StaticConfigs Loaded: dbconfig.properties");
		} catch (Exception e) 
		{
			try {
				properties.load(StaticConfigs.class.getClassLoader().getResourceAsStream(path));
				System.out.println("StaticConfigs Loaded: "+path);
			} catch (IOException e1) 
			{
				System.out.println("StaticConfigs  load exception..."+new File(path).getAbsolutePath());
				e.printStackTrace();
			}
		}
		
	}
	public static Properties getProperties() {
		return properties;
	}
	
}
