package org.myhibernate.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DataHelper 
{
	private Connection connection=null;
	private Statement statement=null;
	private ResultSet resultSet=null;
	private boolean commit=true;
	public DataHelper() {
		//System.out.println("DatabaseHelper Constructor Builded");
	}
	public Connection getConnection() {
		if (connection==null) 
		{ 
			SingleConnection singleConnection=new SingleConnection();
			try {
				connection=singleConnection.getConnectionInstance();
				if (connection != null)
				{
					connection.setAutoCommit(commit);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return connection;
	}
	public void setConnection(Connection connection) 
	{
		this.connection = connection;
	}
	public Statement getStatement() 
	{
		if (statement==null) {
			try {
				statement=getConnection().createStatement();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return statement;
	}
	public void setStatement(Statement statement) {
		this.statement = statement;
	}
	public ResultSet getResultSet() {
		return resultSet;
	}
	public void setResultSet(ResultSet resultSet) {
		this.resultSet = resultSet;
	}
	public void setCommit(boolean c)
	{
		commit=c;
		if (connection != null)
		{
			try {
				connection.setAutoCommit(commit);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	public void rollback()
	{
		try {
			if (connection !=null) {
				connection.rollback();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void commit()
	{
		try {
			if (connection !=null) {
				connection.commit();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public boolean isCommit() {
		return commit;
	}
	public void close()
	{
		try {
			if (resultSet!=null) {
				resultSet.close();
				resultSet=null;
			}
		} catch (Exception e) {
		}
		try {
			if (statement!=null) {
				statement.close();
				statement=null;
			}
		} catch (Exception e) {
		}
		try {
			if (connection!=null) {
				connection.close();
				connection=null;
			}
		} catch (Exception e) {
		}
	}
}
