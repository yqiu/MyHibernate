package org.myhibernate.db;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.Timer;

public class CommonLogs
{
	private static SimpleDateFormat dateFormat_YMD=new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat dateFormat_HH_MM_SS_SSS=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS  ");
	private static SimpleDateFormat dateFormat_HHMMSSSSS=new SimpleDateFormat("HHmmssSSS");
	private static Date date=null;
	private static StringBuffer buffer=new StringBuffer();
	private static StringBuffer buffer_bak=new StringBuffer();
	private static boolean isWrite=false;
	private static Timer timer=null;
	private static String root=""; 
	private static int size=5;
	private static String type="day";
	static
	{
		root="/logs";
		//root=PathUtil.getAutoRootPath();
		File tempFile=new File(root);
		if (!tempFile.exists())
		{
			try {
				tempFile.mkdirs();
				System.out.println("CommonLogs mkdis path:"+root);
			} catch (Exception e) {
			}
		}
//		if (ConfigUtil.getAttribute("log.type")!=null) {
//			type=ConfigUtil.getAttribute("log.type");
//		}
		//2 min/per save
		timer=new Timer(120000, new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if ("day".equals(type))
				{
					saveByDay();
				} else {
					saveBySize();
				}
				
			}
		});
		timer.start();
//		if (ConfigUtil.getAttribute("log.file.size")!=null) {
//			size=Integer.parseInt(ConfigUtil.getAttribute("log.file.size"));
//		}
		Runtime.getRuntime().addShutdownHook(new Thread()
		{
			public void run() 
			{
				if ("day".equals(type))
				{
					saveByDay();
				} else {
					saveBySize();
				}
			}
		});
		System.out.println("CommonLogs static finished");
	}
	public static void main(String[] args)
	{
		System.out.println("ok");
		System.out.println(dateFormat_HHMMSSSSS.format(new Date()));
	}
	public static void add(String log)
	{
		if (isWrite) 
		{
			buffer_bak.append(dateFormat_HH_MM_SS_SSS.format(new Date())+log+"\n");
		} else
		{
			buffer.append(dateFormat_HH_MM_SS_SSS.format(new Date())+log+"\n");
		}
		
	}
	public static void saveByDay()
	{
		if (buffer.length()==0) {return;}
		isWrite=true;
		File logFile=new File(getFilePath());
		if (!logFile.exists())
		{
			try {
				logFile.createNewFile();
			} catch (IOException e) {
				//e.printStackTrace();
			}
		}
		OutputStreamWriter writer=null;
		FileOutputStream fileOutputStream=null;
		try {
			fileOutputStream=new FileOutputStream(logFile, true);
			writer=new OutputStreamWriter(fileOutputStream,"utf-8");
		} catch (FileNotFoundException e) {
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		try {
			writer.write(buffer.toString());
			writer.flush();
			writer.close();
		} catch (IOException e) {
			//e.printStackTrace();
		}
		
		if (buffer.length()>0) {
			buffer.delete(0, buffer.length()-1);
		}
		if (buffer_bak.length()>0) {
			buffer.append(buffer_bak.toString());
			buffer_bak.delete(0,buffer_bak.length()-1);
		}
		
		isWrite=false;
	}
	
	public static void saveBySize()
	{
		if (buffer.length()==0) {return;}
		isWrite=true;
		File logFile=new File(getFilePath());
		if (!logFile.exists())
		{
			try {
				logFile.createNewFile();
			} catch (IOException e) {
				//e.printStackTrace();
			}
		}
		//weather more over size
		if (logFile.length()>(size*1024*1024))
		{
			logFile.renameTo(new File(root+File.separator+"log_"+dateFormat_YMD.format(date)+"."+dateFormat_HHMMSSSSS.format(date)+".log"));
			if (!logFile.exists())
			{
				try {
					logFile.createNewFile();
				} catch (IOException e) {
					//e.printStackTrace();
				}
			}
		}
		OutputStreamWriter writer=null;
		FileOutputStream fileOutputStream=null;
		try {
			fileOutputStream=new FileOutputStream(logFile, true);
			writer=new OutputStreamWriter(fileOutputStream,"utf-8");
		} catch (FileNotFoundException e) {
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		try {
			writer.write(buffer.toString());
			writer.flush();
			writer.close();
		} catch (IOException e) {
			//e.printStackTrace();
		}
		buffer.delete(0, buffer.length()-1);
		buffer.append(buffer_bak.toString());
		buffer_bak.delete(0,buffer_bak.length()-1);
		isWrite=false;
	}
	public static String getRoot() {
		return root;
	}
	public static String getFilePath()
	{
		date=new Date();
		return root+File.separator+"log_"+dateFormat_YMD.format(date)+".log";
	}
	
}
