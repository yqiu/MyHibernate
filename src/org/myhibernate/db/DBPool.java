package org.myhibernate.db;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;
public class DBPool 
{
	  public static String DBTYPE_DB2 = "db2";
	  public static String DBTYPE_ORACLE = "oracle";
	  public static String DBTYPE_MYSQL = "mysql";
	  public static String DBTYPE_SQLSERVER = "sqlserver";
	  
	  private static Properties dbProperties =null;
	  //private static String path = "org/pojo/db/dbconfig.properties";
	   
	  private static String driver="";
	  private static String url="";
	  private static String admin="";
	  private static String password="";
	  private static String type="";
	  private static ThreadLocal<Connection> local=null;
	  private static ComboPooledDataSource dataSource=null;
	  static{
		  System.out.println("----DBPool static----");
		  dbProperties=StaticConfigs.getProperties();
		  load();
		  initPool();
	  }
	  private static void load()
	  {
		    type=dbProperties.getProperty("db.type");
		    driver=dbProperties.getProperty(type+".jdbc.driver");
		    url=dbProperties.getProperty(type+ ".jdbc.url");
		    admin=dbProperties.getProperty(type+".jdbc.username");
		    password=dbProperties.getProperty(type+".jdbc.password");
		    System.out.println("--------------------DBPool load--------------------------");
		    System.out.println(DBPool.class.getProtectionDomain().getCodeSource().getLocation().getFile());
		    System.out.println("type="+type);
		    System.out.println("driver="+driver);
		    System.out.println("url="+url);
		    System.out.println("admin="+admin);
		    System.out.println("password="+password);
		    System.out.println("--------------------DBPool load--------------------------");
	  }
	  private static void initPool()
	  {
		  dataSource=new ComboPooledDataSource();
		  try {
			dataSource.setDriverClass(driver);
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
		  try {
			dataSource.setJdbcUrl(url);
			  dataSource.setUser(admin);
			  dataSource.setPassword(password);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		  
		  try {
			dataSource.setInitialPoolSize(Integer.parseInt(dbProperties.getProperty("initialPoolSize")));
			  dataSource.setMinPoolSize(Integer.parseInt(dbProperties.getProperty("minPoolSize")));
			  dataSource.setMaxPoolSize(Integer.parseInt(dbProperties.getProperty("maxPoolSize")));
			//  dataSource.setCheckoutTimeout(Integer.parseInt(dbProperties.getProperty("checkOutTimeOut")));
			  dataSource.setAcquireIncrement(Integer.parseInt(dbProperties.getProperty("setAcquireIncrement")));
			  dataSource.setIdleConnectionTestPeriod(Integer.parseInt(dbProperties.getProperty("idleConnectionTestPeriod")));
			  dataSource.setMaxIdleTime(Integer.parseInt(dbProperties.getProperty("maxIdleTime")));
			  dataSource.setAutoCommitOnClose(Boolean.parseBoolean(dbProperties.getProperty("autoCommitOnClose")));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		  
	  }
	  
	  public synchronized static Connection getConnection()
	  {
		  
		  Connection tempConnection=null;
		  try {
			  tempConnection=dataSource.getConnection();
			  local=new ThreadLocal<Connection>();
			  local.set(tempConnection);
			return tempConnection;
		} catch (SQLException e) {
			System.out.println("Exception Class Path:"+DBPool.class.getProtectionDomain().getCodeSource().getLocation().getFile());
			try {
				System.out.println("Max Connections="+dataSource.getMaxPoolSize());
				System.out.println("getNumConnections="+dataSource.getNumConnections());
				System.out.println("getNumBusyConnections="+dataSource.getNumBusyConnections());
				System.out.println("getNumIdleConnections="+dataSource.getNumIdleConnections());
			} catch (Exception e2) {
			}
			//System.out.println(dataSource.getNum);
			e.printStackTrace();
		}
		return null;
	  }
	  public synchronized static Connection getConnection(String jndiName)
	  {
		  Context initContext=null;
			DataSource ds =null;
			try {
				initContext = new InitialContext();
				ds = (DataSource) initContext.lookup(jndiName);
				Connection tempConnection=ds.getConnection();
				local=new ThreadLocal<Connection>();
				local.set(tempConnection);
				return tempConnection;
			} catch (Exception e) {
				System.out.println("Exception Class Path:"+DBPool.class.getProtectionDomain().getCodeSource().getLocation().getFile());
				System.out.println("Exception for jndiName=========="+jndiName);
				e.printStackTrace();
			}
		  
		  return null;
	  }
	  public static void close(Connection connection)
	  {
		  if (connection!=null) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	  }
	  public static void close()
	  {
		  if (local.get()!=null)
		  {
			  try {
				local.get().close();
				System.out.println("DBPool closed threadlocal connection ");
				
			} catch (SQLException e) { 
				e.printStackTrace();
			}
		  }
	  }
//	  public static void main(String[] args) {
//		System.out.println(getConnection());
//		close();
//		System.out.println("finish");
//	}
}
