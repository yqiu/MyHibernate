package org.myhibernate.auth;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AuthObject
{
	private String id="";
	private String pid="";
	private String title="";
	private String inherit="true";
	 
	private List<String> users=new ArrayList<String>();
	private List<String> creators=new ArrayList<String>();
	private List<String> editors=new ArrayList<String>();
	private List<String> removers=new ArrayList<String>();
	private List<String> managers=new ArrayList<String>();
	
	private List<String> children=new ArrayList<String>();
	
	private String className="";
	private String objectId=""; 
	
	private String createDate="";
	private String publishDate="";
	private String expireDate="";
	private String gDate1="";
	private String gDate2="";
	private String createTime="0";
	private String publishTime="0";
	private String expireTime="0";
	private String gTime1="0";
	private String gTime2="0";
	
	private String sort1="";
	private String sort2="";
	private String ext1="";
	private String ext2="";
	private String ext3="";
	private String ext4="";
	private String ext5="";
	
	public String getId() {
		if ((id==null)||("".equals(id))) {
			id=UUID.randomUUID().toString();
		}
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	
	public List<String> getUsers() {
		return users;
	}
	public void setUsers(List<String> users) {
		this.users = users;
	}
	public List<String> getCreators() {
		return creators;
	}
	public void setCreators(List<String> creators) {
		this.creators = creators;
	}
	public List<String> getEditors() {
		return editors;
	}
	public void setEditors(List<String> editors) {
		this.editors = editors;
	}
	public List<String> getRemovers() {
		return removers;
	}
	public void setRemovers(List<String> removers) {
		this.removers = removers;
	}
	public List<String> getManagers() {
		return managers;
	}
	public void setManagers(List<String> managers) {
		this.managers = managers;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getObjectId() {
		return objectId;
	}
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getPublishDate() {
		return publishDate;
	}
	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}
	public String getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	
	
	public String getSort1() {
		return sort1;
	}
	public void setSort1(String sort1) {
		this.sort1 = sort1;
	}
	public String getSort2() {
		return sort2;
	}
	public void setSort2(String sort2) {
		this.sort2 = sort2;
	}
	
	public List<String> getChildren() {
		return children;
	}
	public void setChildren(List<String> children) {
		this.children = children;
	}
	public String getgDate1() {
		return gDate1;
	}
	public void setgDate1(String gDate1) {
		this.gDate1 = gDate1;
	}
	public String getgDate2() {
		return gDate2;
	}
	public void setgDate2(String gDate2) {
		this.gDate2 = gDate2;
	}
	
	public String getExt1() {
		return ext1;
	}
	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}
	public String getExt2() {
		return ext2;
	}
	public void setExt2(String ext2) {
		this.ext2 = ext2;
	}
	public String getExt3() {
		return ext3;
	}
	public void setExt3(String ext3) {
		this.ext3 = ext3;
	}
	public String getExt4() {
		return ext4;
	}
	public void setExt4(String ext4) {
		this.ext4 = ext4;
	}
	public String getExt5() {
		return ext5;
	}
	public void setExt5(String ext5) {
		this.ext5 = ext5;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getInherit() {
		return inherit;
	}
	public void setInherit(String inherit) {
		this.inherit = inherit;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getPublishTime() {
		return publishTime;
	}
	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}
	public String getExpireTime() {
		return expireTime;
	}
	public void setExpireTime(String expireTime) {
		this.expireTime = expireTime;
	}
	public String getgTime1() {
		return gTime1;
	}
	public void setgTime1(String gTime1) {
		this.gTime1 = gTime1;
	}
	public String getgTime2() {
		return gTime2;
	}
	public void setgTime2(String gTime2) {
		this.gTime2 = gTime2;
	}
}
