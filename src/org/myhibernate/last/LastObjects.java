package org.myhibernate.last;

import java.util.HashMap;
import java.util.Map;

public class LastObjects 
{
	private static Map<String, ObjectList> map=new HashMap<String, ObjectList>();
	
	public synchronized static ObjectList get(Class cls)
	{
		if (map.get(cls.getName())==null) {
			map.put(cls.getName(), new ObjectList(cls.getName()));
		}
		return map.get(cls.getName());
	}
	 
}
