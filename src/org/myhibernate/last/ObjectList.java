package org.myhibernate.last;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ObjectList 
{
	private String className="";
	private List<Object> idList=new ArrayList<Object>();
	private Map<Object, Integer> idIndexMap=new HashMap<Object, Integer>();
	private Map<Object, Long> idTimeMap=new HashMap<Object, Long>();
	public ObjectList() {
	}
	public ObjectList(String className) {
		this.className = className;
	}
	 
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public List<Object> getIdList() {
		return idList;
	}
	public Map<Object, Integer> getIdIndexMap() {
		return idIndexMap;
	}
	public Map<Object, Long> getIdTimeMap() {
		return idTimeMap;
	}
	public void add(Object id)
	{
		if (idIndexMap.get(id)!=null) {
			idList.remove(idIndexMap.get(id));
		}
		idList.add(id);
		idIndexMap.put(id, idList.size()-1);
		idTimeMap.put(id,System.currentTimeMillis());
	}
	
}
