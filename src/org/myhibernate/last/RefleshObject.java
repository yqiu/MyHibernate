package org.myhibernate.last;

import java.util.List;

public class RefleshObject 
{
	private ObjectList list=null;
	public RefleshObject(Class cls) {
		list=LastObjects.get(cls);
	}

	public void reflesh(Object id)
	{
		list.add(id);
	} 
	public void reflesh(List<Object> idList)
	{
		if ((idList==null)||(idList.size()==0)) {
			return;
		}
		for (int i = 0; i < idList.size(); i++) 
		{
			list.add(idList.get(i));
		}
	}
	public ObjectList getObjectList(Class cls)
	{
		return list;
	}
}
