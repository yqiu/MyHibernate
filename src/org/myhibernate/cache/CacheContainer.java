package org.myhibernate.cache;

import java.util.HashMap;
import java.util.Map;

public class CacheContainer 
{
	private Class cls=null;
	private boolean busy=false;
	private Map<Object, CacheItem> map=new HashMap<Object, CacheItem>();
	public CacheContainer(Class cls) 
	{
		this.cls = cls; 
	}
	public  synchronized Object get(Object id)
	{
		CacheItem item=map.get(id+"");
		if (item != null)
		{
			return item.getObject();
		}
		return null;
	}
	public  synchronized void set(Object id,Object obj)
	{
		if ((obj==null)||(id==null)) {
			return;
		}
		if (!getCls().getSimpleName().equals(obj.getClass().getSimpleName()))
		{
			throw new RuntimeException("Can not cast from "+obj.getClass()+" to "+getCls());
		}
		map.put(id+"", new CacheItem(obj));
	}
	public  synchronized CacheItem getItem(Object id)
	{
		return map.get(id);
	}
	public  synchronized void setItem(Object id,CacheItem item)
	{
		map.put(id, item);
	}
	public synchronized void remove(Object id)
	{
		map.remove(id+"");
	}
	public  Map<Object, CacheItem> getMap() {
		return map;
	}
	public void setBusy(boolean busy) {
		this.busy = busy;
	}
	public boolean isBusy() {
		return busy;
	}
	public Class getCls() {
		return cls;
	}
	public  void clear()
	{
		map.clear();
	}
}
