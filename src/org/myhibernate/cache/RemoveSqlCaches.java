package org.myhibernate.cache;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class RemoveSqlCaches 
{
	private String className="";
	public RemoveSqlCaches() {
	}
	public RemoveSqlCaches(Class cls) 
	{
		this.className = cls.getName();
		CacheContainer cacheContainer=CacheContainers.getCache(ArrayList.class);
		if (SqlQueryMap.get(className)!=null) 
		{ 
			Map<String,String> tempMap=SqlQueryMap.get(className);
			Iterator<String> iterator=tempMap.keySet().iterator();
			while (iterator.hasNext())
			{
				cacheContainer.remove(iterator.next());
			}
			tempMap.clear();
		}
	}
}
