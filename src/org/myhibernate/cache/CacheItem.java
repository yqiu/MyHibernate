package org.myhibernate.cache;

public class CacheItem 
{
	private Object object=null;
	private boolean isExpireAble=true;
	private boolean isLocked=false;
	private long createTime=0l;
	private int expire=36000000;//default 10 hour
	private long lastTime=0l;
	private boolean isClearWithExpire=true;
	public CacheItem(Object object) 
	{
		this.object = object; 
		this.createTime=System.currentTimeMillis();
		this.lastTime=this.createTime;
	}
	public Object getObject() {
		reflesh();
		return object;
	} 
	public void setObject(Object object) {
		this.object = object;
	}
	public long getCreateTime() {
		return createTime;
	}
	public boolean isLocked() {
		return isLocked;
	}
	public void lock()
	{
		this.isLocked=true;
	}
	public void unLock()
	{
		this.isLocked=false;
	}
	public void setExpire(int expire) {
		this.expire = expire;
	}
	public int getExpire() {
		return expire;
	}
	public long getLastTime() {
		return lastTime;
	}
	public void reflesh()
	{
		lastTime=System.currentTimeMillis();
	}
	public void setClearWithExpire(boolean isClearWithExpire) {
		this.isClearWithExpire = isClearWithExpire;
	}
	public void setExpireAble(boolean isExpireAble) {
		this.isExpireAble = isExpireAble;
	}
	public boolean isExpireAble() {
		return isExpireAble;
	}
	public boolean isClearWithExpire() {
		return isClearWithExpire;
	}
}
