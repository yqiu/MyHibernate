package org.myhibernate.cache;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.Timer;

public class TimeClear 
{
	private static Timer timer=null;
	public static void init()
	{
		timer=new Timer(300000, new ActionListener() // 5min
		{
			public void actionPerformed(ActionEvent event) 
			{
				Map<String, CacheContainer> containerMap=CacheContainers.getMap();
				Iterator<String> iterator=containerMap.keySet().iterator();
				String className="";
				CacheContainer container=null;
				long current=System.currentTimeMillis();
				while(iterator.hasNext())
				{ 
					className=iterator.next();
					container=containerMap.get(className);
					if (!container.isBusy())
					{
						List<String> idList=new ArrayList<String>();
						Map<Object, CacheItem> maps=container.getMap();
						Iterator iterator2=maps.keySet().iterator();
						CacheItem item=null;
						String id="";
						while (iterator2.hasNext())
						{
							id=iterator2.next()+"";
							item=maps.get(id);
							if (item.isExpireAble())
							{
								if (item.isClearWithExpire()) 
								{
									if ((current-item.getCreateTime())>item.getExpire())
									{
										idList.add(id);
									}
								} else {
									if ((current-item.getLastTime())>item.getExpire())
									{
										idList.add(id);
									}
								}
							} 
						}//endwhile
						for (int i = 0; i < idList.size(); i++) 
						{
							maps.remove(idList.get(i));
						}
					}//endif
				}
			}
		});
		timer.start();
		Runtime.getRuntime().addShutdownHook(new Thread(){
			@Override
			public void run() {
				timer.stop();
			}
		});
	}
	public static Timer getTimer() {
		return timer;
	}
}
