package org.myhibernate.cache;

import java.util.HashMap;
import java.util.Map;

public class SqlQueryMap 
{
	private static Map<String,Map<String, String>> sqlCacheMap=new HashMap<String, Map<String, String>>();
	public synchronized static void set(Class cls,String sqlCacheSql)
	{
		if (sqlCacheMap.get(cls.getName())==null) {
			sqlCacheMap.put(cls.getName(),new HashMap<String, String>());
		}
		sqlCacheMap.get(cls.getName()).put(sqlCacheSql, "");
	}
	public synchronized static Map<String, String> get(String className)
	{
		return sqlCacheMap.get(className);
	}
	  
}
