package org.myhibernate.cache;

import java.util.HashMap;
import java.util.Map;

public class CacheContainers 
{
	private static Map<String, CacheContainer> map=new HashMap<String, CacheContainer>();
	static{
		TimeClear.init();
	}
	public static synchronized CacheContainer  getCache(Class cls)
	{
		CacheContainer c=map.get(cls.getName());
		if (c==null) { 
			c=new CacheContainer(cls);
			map.put(cls.getName(), c);
		}
		return c;
	}
	public static Map<String, CacheContainer> getMap() {
		return map;
	}
	
}
