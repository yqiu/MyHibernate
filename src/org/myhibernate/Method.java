package org.myhibernate;

import java.sql.Connection;
import java.util.Map;

import org.myhibernate.db.DataHelper;

public interface Method<T>
{
	public void init(Class<T> cl,Connection connection);
	public void init(Class<T> cl,DataHelper helper);
	public void init(Class<T> cl,Connection connection,DataHelper helper);
	public Object execute(Object obj,Map<String,Object> map) throws Exception;
	public DataHelper getDataHelper();
	public void close(); 
}