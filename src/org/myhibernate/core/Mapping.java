package org.myhibernate.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Mapping 
{
	private String name="";
	private String table="";
	private String id_Field="";
	private String id_Set="";
	private String id_Get="";
	private String id_Column=""; 
	private List<String> propertyList=new ArrayList<String>();
	private List<String> propertySetList=new ArrayList<String>();
	private List<String> propertyGetList=new ArrayList<String>();
	private List<String> propertyColList=new ArrayList<String>();
	private Map<String, String> method_set_map=new HashMap<String, String>();
	private Map<String, String> method_get_map=new HashMap<String, String>();
	private Map<String, String> propertyColMap=new HashMap<String, String>();
	private String columns="";
	public Mapping() {
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTable() {
		return table;
	}
	public void setTable(String table) {
		this.table = table;
	}
	public String getId_Set() {
		return id_Set;
	}
	public void setId_Set(String idSet) {
		id_Set = idSet;
	}
	public String getId_Get() {
		return id_Get;
	}
	public void setId_Get(String idGet) {
		id_Get = idGet;
	}
	public String getId_Column() {
		return id_Column;
	}
	public void setId_Column(String idColumn) {
		id_Column = idColumn;
	}
	public List<String> getPropertySetList() {
		return propertySetList;
	}
	public void setPropertySetList(List<String> propertySetList) {
		this.propertySetList = propertySetList;
	}
	public List<String> getPropertyGetList() {
		return propertyGetList;
	}
	public void setPropertyGetList(List<String> propertyGetList) {
		this.propertyGetList = propertyGetList;
	}
	public List<String> getPropertyColList() {
		return propertyColList;
	}
	public void setPropertyColList(List<String> propertyColList) {
		this.propertyColList = propertyColList;
	}
	public void initColumns()
	{
		StringBuffer buffer=new StringBuffer();
		for (int i = 0; i < getPropertyColList().size(); i++)
		{
			if (0==i)
			{
				buffer.append(getPropertyColList().get(i));
			} else {
				buffer.append(","+getPropertyColList().get(i));
			}
		}
		columns=buffer.toString();
	}
	public String getColumns() {
		return columns;
	}
	public List<String> getPropertyList() {
		return propertyList;
	}
	public void setPropertyList(List<String> propertyList) {
		this.propertyList = propertyList;
	}
	public String getId_Field() {
		return id_Field;
	}
	public void setId_Field(String idField) {
		id_Field = idField;
	}
	public Map<String, String> getMethod_get_map() {
		return method_get_map;
	}
	public Map<String, String> getMethod_set_map() {
		return method_set_map;
	}
	public Map<String, String> getPropertyColMap() {
		return propertyColMap;
	}
	@Override
	public String toString() 
	{
		return getId_Field()+","+getId_Column()+","+getId_Set()+","+getId_Get()+";"+getColumns()+";"+getMethod_get_map()+";"+getMethod_set_map();
	}
}
