package org.myhibernate.core;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class LanguageHandle 
{
	public static String TYPE_DB2="'";
	public static String TYPE_ORACLE="'";
	public static String TYPE_MYSQLS="'";
	private static String type="'";
	private static String DB_TYPE="";
	private static Map<String, String> typeMap=new HashMap<String, String>();
	static{ 
		typeMap.put("mysql", "'");
		typeMap.put("db2", "'");
		typeMap.put("oracle", "'");
	}
	public LanguageHandle() {
	}
	public static void setDB_TYPE(String dBTYPE) {
		//System.out.println("LanguageHandle set type="+dBTYPE);
		DB_TYPE = dBTYPE;
		type=typeMap.get(dBTYPE);
	}
	public static String getDB_TYPE() {
		return DB_TYPE;
	}
	public LanguageHandle(String type) 
	{
		this.type=type;
	}
	public static String getType() {
		return type;
	}
	public Object handle(Object object)
	{
		StringBuffer buffer=new StringBuffer();
		if ((object instanceof String)||(object instanceof StringBuffer))// string
		{
			buffer.append(getType()+object+getType());
			return buffer.toString();
		}else if (object instanceof List)//list
		{
			List list=(List)object;
			buffer.append(getType());
			for (int i = 0; i < list.size(); i++)
			{
				if (0==i)
				{
					buffer.append(Seperators.SEP1+list.get(i)+Seperators.SEP1);
				} else {
					buffer.append(list.get(i)+Seperators.SEP1);
				}
			}
			buffer.append(getType());
			return buffer.toString();
		}else if (object instanceof Map)//map
		{
			Map map=(Map)object;
			Iterator<String> iterator=map.keySet().iterator();
			String key="";
			buffer.append(getType());
			int count=0;
			while (iterator.hasNext())
			{
				key=iterator.next();
				if (0==count)
				{
					buffer.append(Seperators.SEP1+key+"="+map.get(key)+Seperators.SEP1);
				} else {
					buffer.append(key+"="+map.get(key)+Seperators.SEP1);
				}
				count++;
			}
			buffer.append(getType());
			return buffer.toString();
		}else // other
		{
			return object;
		}
		
	}
}
