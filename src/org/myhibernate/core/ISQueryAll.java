package org.myhibernate.core;

import java.util.HashMap;
import java.util.Map;

public class ISQueryAll 
{
	private static Map<String,Boolean> isAllMap=new HashMap<String, Boolean>();
	public static void setQueryAll(Class cls)
	{
		isAllMap.put(cls.getSimpleName(),true);
	}
	public static boolean isQueryAll(Class cls)
	{ 
		if (isAllMap.get(cls.getSimpleName())==null) {
			return false;
		}
		return true;
	}
	public static  Map<String,Boolean> getMap() {
		return isAllMap;

	}
	
}
