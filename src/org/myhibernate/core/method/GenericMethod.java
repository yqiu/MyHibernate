package org.myhibernate.core.method;

import java.sql.Connection;
import java.util.Map;

import org.myhibernate.Method;
import org.myhibernate.core.LanguageHandle;
import org.myhibernate.core.Mapping;
import org.myhibernate.core.Mappings;
import org.myhibernate.db.DataHelper;


public class GenericMethod<T> implements Method<T>
{
	private Class cl=null;
	private Mapping mapping=null;
	private StringBuffer buffer=new StringBuffer();
	private LanguageHandle handle=null;
	private Connection connection=null;
	private boolean sqlShow=false;
	private String model="";
	private DataHelper helper=null;
	public void init(Class cl,Connection connection) 
	{
		this.cl=cl; 
		this.connection=connection;
		this.sqlShow=Mappings.sqlShow;
		this.model=Mappings.model;
		mapping=Mappings.getMapping(cl);
		handle=new LanguageHandle();
		getMapping().setTable(getModel()+getMapping().getTable());
		if ((connection!=null)&&(getDataHelper()!=null)) {
			getDataHelper().setConnection(connection);
		}
	}
	public void init(Class cl,DataHelper helper) 
	{
		System.out.println("init helper="+helper);
		this.cl=cl;
		this.sqlShow=Mappings.sqlShow;
		this.model=Mappings.model;
		this.helper=helper;
		mapping=Mappings.getMapping(cl);
		handle=new LanguageHandle();
		getMapping().setTable(getModel()+getMapping().getTable());
	}
	public void init(Class cl,Connection connection,DataHelper helper) 
	{
		this.cl=cl;
		this.connection=connection;
		this.sqlShow=Mappings.sqlShow;
		this.model=Mappings.model;
		this.helper=helper;
		mapping=Mappings.getMapping(cl);
		handle=new LanguageHandle();
		getMapping().setTable(getModel()+getMapping().getTable());
		if ((connection!=null)) {
			if (getDataHelper()==null) {
				this.helper=new DataHelper();
			}
			getDataHelper().setConnection(connection);
		}
	}
	public Object execute(Object obj, Map<String, Object> map) throws Exception 
	{
		System.out.println("GenericPoMethod execute...");
		return null;
	}
	public StringBuffer getBuffer() {
		return buffer;
	}
	public String getSql()
	{
//		if (sqlShow) {
//			System.out.println(buffer.toString());
//		}
		return getBuffer().toString();
	}
	public Mapping getMapping() {
		return mapping;
	}
	public Class getCl() {
		return cl;
	}
	public LanguageHandle getHandle() {
		return handle;
	}
	public Connection getConnection() {
		return connection;
	}
	public String getModel() {
		return model;
	}
	public void close() {
		
	}
	public DataHelper getDataHelper() {
		System.out.println("get helper:"+helper);
		return this.helper;
	}

}
