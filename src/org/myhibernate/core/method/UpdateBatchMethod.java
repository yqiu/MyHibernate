package org.myhibernate.core.method;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

import org.myhibernate.core.method.CacheQuery;
import org.myhibernate.core.method.GenericMethod;
import org.myhibernate.db.DataHelper;

public class UpdateBatchMethod<T> extends GenericMethod<T>
{
	private CacheQuery cacheQuery=null;
	public void init(Class cls, Connection connection) 
	{
		super.init(cls, connection);
		cacheQuery=new CacheQuery(connection, cls); 
	}
	@Override
	public void init(Class cl, DataHelper helper) {
		super.init(cl, helper);
		cacheQuery=new CacheQuery(cl, helper);
	}
	@Override 
	public void init(Class cl, Connection connection, DataHelper helper) {
		super.init(cl, connection, helper);
		cacheQuery=new CacheQuery(cl, connection, helper);
	}
	@Override
	public Object execute(Object lists, Map<String, Object> map) throws Exception 
	{
		boolean flag=false;
		List<T> list=(List<T>) lists;
		try 
		{
			getDataHelper().getConnection().setAutoCommit(false);// not commit
			///////////////////////////////////////
			// update to database
			for (int k = 0; k < list.size(); k++) 
			{
				Object obj=list.get(k);
				if (getBuffer().length()>0) {
					getBuffer().delete(0,getBuffer().length());
				}
				//
				PropertyDescriptor descriptor=null;
				// update to cache
				 descriptor= new PropertyDescriptor(getMapping().getId_Field(), getCl());
				 Method rM = descriptor.getReadMethod();
				 Object idValue=rM.invoke(obj);
				 //update to database
				 List<String> fieldList=getMapping().getPropertyList();
					List<String> columnList=getMapping().getPropertyColList();
					
					String fieldName="";
					Object value=null;
					getBuffer().append("update "+getMapping().getTable()+" set ");
					for (int i = 0; i < fieldList.size(); i++) 
					{
						fieldName=fieldList.get(i);
						try {
							// descriptor= new PropertyDescriptor(fieldName, getCl());
							 //rM = descriptor.getReadMethod();
							 rM=getCl().getDeclaredMethod(getMapping().getPropertyGetList().get(i));
							 value=rM.invoke(obj);
							 if (0==i)
							 {
								 getBuffer().append(columnList.get(i)+"="+getHandle().handle(value));
							} else {
								getBuffer().append(","+columnList.get(i)+"="+getHandle().handle(value));
							}
						} catch (Exception e) {
						}
					}//endfor
					getBuffer().append(" where "+getMapping().getId_Column()+"="+getHandle().handle(idValue));
					
					String sql=getSql();
					cacheQuery.executeSql(sql);
				
			}
			
			flag=true;
			////////////////////////////////////////////////
			if (getDataHelper().isCommit()) {
				getDataHelper().getConnection().commit();//commit all
			}
		} catch (Exception e) 
		{
			flag=false;
			getDataHelper().getConnection().rollback();// rollback
			e.printStackTrace();
			throw new RuntimeException(e.toString());
		}finally
		{
			if (getDataHelper().isCommit()) {
				getDataHelper().getConnection().setAutoCommit(true);// set commit true
			}
		}
		
		return flag;
	}
	@Override
	public void close() {
		cacheQuery.close();
	}
}
