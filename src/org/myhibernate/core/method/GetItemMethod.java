package org.myhibernate.core.method;

import java.sql.Connection;
import java.util.Map;

import org.myhibernate.core.method.CacheQuery;
import org.myhibernate.core.method.GenericMethod;

public class GetItemMethod<T> extends GenericMethod<T>
{
	private CacheQuery cacheQuery=null;
	@Override
	public void init(Class cl, Connection connection) {
		super.init(cl, connection);
		cacheQuery=new CacheQuery(connection, cl);
	}
	@Override
	public Object execute(Object obj, Map<String, Object> map) throws Exception
	{
		Object object=cacheQuery.getObjectItem(obj);
		return object;
	} 
}
