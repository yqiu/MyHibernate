package org.myhibernate.core.method;

import java.sql.Connection;
import java.util.Map;

import org.myhibernate.core.method.CacheQuery;
import org.myhibernate.core.method.GenericMethod;
import org.myhibernate.db.DataHelper;

public class GetMethod<T> extends GenericMethod<T>
{
	private CacheQuery cacheQuery=null;
	@Override
	public void init(Class cl, Connection connection) {
		super.init(cl, connection);
		cacheQuery=new CacheQuery(connection, cl);
	}
	@Override
	public void init(Class cl, DataHelper helper) {
		super.init(cl, helper);
		cacheQuery=new CacheQuery(cl, helper);
	}
	@Override 
	public void init(Class cl, Connection connection, DataHelper helper) {
		super.init(cl, connection, helper);
		cacheQuery=new CacheQuery(cl, connection, helper);
	}
	@Override
	public Object execute(Object obj, Map<String, Object> map) throws Exception
	{
		Object object=cacheQuery.getObject(obj);
		return object;
	}
}
