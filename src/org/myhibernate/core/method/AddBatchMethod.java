package org.myhibernate.core.method;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

import org.myhibernate.cache.CacheContainer;
import org.myhibernate.cache.CacheContainers;
import org.myhibernate.cache.RemoveSqlCaches;
import org.myhibernate.core.method.CacheQuery;
import org.myhibernate.core.method.GenericMethod;
import org.myhibernate.db.DataHelper;

public class AddBatchMethod<T> extends GenericMethod<T>
{
	private CacheQuery cacheQuery=null;
	public void init(Class cls, Connection connection) 
	{
		super.init(cls, connection);
		cacheQuery=new CacheQuery(connection, cls); 
	} 
	@Override
	public void init(Class cl, DataHelper helper) {
		super.init(cl, helper);
		cacheQuery=new CacheQuery(cl, helper);
	}
	@Override
	public void init(Class cl, Connection connection, DataHelper helper) {
		super.init(cl, connection, helper);
		cacheQuery=new CacheQuery(cl, connection, helper);
	}
	public Object execute(Object dataList, Map<String, Object> map) throws Exception 
	{
		if (dataList == null) {
			return false;
		}
		List list=(List) dataList;
		boolean flag=false;
		try 
		{
			getDataHelper().getConnection().setAutoCommit(false);// not commit
			///////////////////////////////////////
			for (int k = 0; k < list.size(); k++) 
			{
				Object obj=list.get(k);
				if (getBuffer().length()>0) {
					getBuffer().delete(0,getBuffer().length());
				}
				PropertyDescriptor descriptor=null;
				 descriptor= new PropertyDescriptor(getMapping().getId_Field(), getCl());
				 Method rM = descriptor.getReadMethod();
				 Object idValue=rM.invoke(obj);
				
				// add to database
				
					List<String> fieldList=getMapping().getPropertyList();
					String fieldName="";
					Object value=null;
					getBuffer().append("insert into "+getMapping().getTable()+"("+getMapping().getColumns()+") values(");
					for (int i = 0; i < fieldList.size(); i++) 
					{
						fieldName=fieldList.get(i);
						 
							 //descriptor= new PropertyDescriptor(fieldName, getCl());
							 //rM = descriptor.getReadMethod();
							 rM=getCl().getDeclaredMethod(getMapping().getPropertyGetList().get(i));
							 value=rM.invoke(obj);
							 if (0==i)
							 {
								 getBuffer().append(getHandle().handle(value));
							} else {
								getBuffer().append(","+getHandle().handle(value));
							}
						
					}//end for object fields
					getBuffer().append(")");
					String sql=getSql();
					
					cacheQuery.executeSql(sql);
					//add to cache
					CacheContainer cacheContainer=CacheContainers.getCache(getCl());
					cacheContainer.set(idValue, obj);
				
			}//end for data list
			//delete sql cache
			new RemoveSqlCaches(getCl());
			flag=true;
			////////////////////////////////////////////////
			if (getDataHelper().isCommit()) {
				getDataHelper().getConnection().commit();//commit all
			}
		} catch (Exception e) 
		{
			flag=false;
			getDataHelper().getConnection().rollback();// rollback
			e.printStackTrace();
			throw new RuntimeException(e.toString());
		}finally
		{
			if (getDataHelper().isCommit()) {
				getDataHelper().getConnection().setAutoCommit(true);// set commit true
			}
		}
		
		return flag;
	}
	public void close() {
		cacheQuery.close();
	}
}
