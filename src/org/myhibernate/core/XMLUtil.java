package org.myhibernate.core;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.xml.sax.InputSource;

public class XMLUtil 
{
	 public   String readUrlContent(String urlStr,String charactor)
	    {
			if(charactor==null){charactor="utf-8";}
	    	StringBuffer document = new StringBuffer();
	        try  
	        {
	            java.net.URL url = new java.net.URL(urlStr);
	            java.net.URLConnection conn = url.openConnection();
	            java.io.BufferedReader reader = new java.io.BufferedReader(new java.io.InputStreamReader(conn.getInputStream(),charactor));
	            String line = null;
	            while ((line = reader.readLine()) != null)
	            {
	            	document.append(line);
	            	//System.out.println(line);
	            }
	            reader.close();
	        }
	        catch (Exception e) 
	        {
	            e.printStackTrace(); 
	        }
	        
	        return document.toString();
	    }
	 public  String readFile(String path,String charactor)
	 {
		 StringBuffer buffer = new StringBuffer("");
	        try 
	        {
	            java.io.BufferedReader reader = new java.io.BufferedReader(new java.io.InputStreamReader(new FileInputStream(path),charactor));
	            String line = null;
	            while ((line = reader.readLine()) != null)
	            {
	            	buffer.append(line);
	            }
	            reader.close();
	        }
	        catch (Exception e) 
	        {
	            e.printStackTrace(); 
	        }
	        
	        return buffer.toString();
	 }
	 public  String readFile(InputStream is,String charactor)
	 {
		 StringBuffer buffer = new StringBuffer("");
	        try 
	        {
	            java.io.BufferedReader reader = new java.io.BufferedReader(new java.io.InputStreamReader(is,charactor));
	            String line = null;
	            while ((line = reader.readLine()) != null)
	            {
	            	buffer.append(line);
	            }
	            reader.close();
	        }
	        catch (Exception e) 
	        {
	            e.printStackTrace(); 
	        }
	        
	        return buffer.toString();
	 }
	 public   List<Element> getXmlChildren(String xml,String node) throws Exception
		{
			StringReader read = new StringReader(xml);
			InputSource source = new InputSource(read);
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(source);
			Element root = doc.getRootElement();
			List<Element> children = root.getChildren(node);
			return children;
		}
	 public  String getXmlNode(String xml,String node) throws Exception
	{
			String value = null;
			StringReader read = new StringReader(xml);
			InputSource source = new InputSource(read);
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(source);
			Element root = doc.getRootElement();
			List child = root.getChildren();
			//System.out.println(child.size());
			Element e = null;
			for (int i = 0; i < child.size(); i++) 
			{
				e = (Element) child.get(i);
				//System.out.println(e.getName()+"="+e.getText());
				if (node.trim().equals(e.getName()))
				{
					value=e.getText().trim();
					break;
				}
			}
			return value;
		}
		public  Element getXmlRoot(String xml) throws Exception
		{
			String value = null;
			StringReader read = new StringReader(xml);
			InputSource source = new InputSource(read);
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(source);
			Element root = doc.getRootElement();
			return root;
		}
	 public static void main(String[] args) 
	 {
		
	}
}
