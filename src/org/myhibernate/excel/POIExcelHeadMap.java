package org.myhibernate.excel;

import java.util.LinkedList;

public class POIExcelHeadMap 
{
	private LinkedList<String> nameList=new LinkedList<String>();
	private LinkedList<String> titleList=new LinkedList<String>();
	public POIExcelHeadMap add(String name,String title)
	{
		nameList.add(name);
		titleList.add(title);
		return this;
	}
	public LinkedList<String> getNameList() {
		return nameList;
	}
	public LinkedList<String> getTitleList() {
		return titleList;
	}
}
