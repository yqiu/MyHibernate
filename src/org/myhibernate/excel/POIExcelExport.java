package org.myhibernate.excel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class POIExcelExport 
{
	private POIExcelHeadMap headMap=null;
	private List<Map<String, Object>> dataMapList=null;
	private HSSFWorkbook workbook=null;
	public POIExcelExport(POIExcelHeadMap headMap, List<Map<String, Object>> dataMapList) {
		this.headMap = headMap;
		this.dataMapList = dataMapList;
		workbook=new  HSSFWorkbook();
		HSSFSheet sheet=workbook.createSheet();
		//add head row
		HSSFRow headRow=sheet.createRow(0);
		List<String> titleList=headMap.getTitleList();
		String title="";
		HSSFCell cell=null;
		for (int i = 0; i < titleList.size(); i++) 
		{
			title=titleList.get(i);
			cell=headRow.createCell(i);
			cell.setCellValue(title+"");
		}
		//add data rows
		List<String> nameList=headMap.getNameList();
		String name="";
		for (int i = 0; i < dataMapList.size(); i++) 
		{
			Map<String, Object> dataMap=dataMapList.get(i);
			HSSFRow dataRow=sheet.createRow(i+1);
			for (int j = 0; j < nameList.size(); j++) 
			{
				name=nameList.get(j);
				HSSFCell dataCell=dataRow.createCell(j);
				dataCell.setCellValue(dataMap.get(name)+"");
			}
		}
		
	}
	public void save(String saveExcelPath)
	{
		File excelFile=new File(saveExcelPath);
		File parentFile=excelFile.getParentFile();
		if (!parentFile.exists())
		{
			parentFile.mkdirs();
		}
		try {
			FileOutputStream fileOut = new FileOutputStream(excelFile);     
			workbook.write(fileOut);     
			fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}

	public void download(HttpServletResponse response,String fileName)
	{
			if (!fileName.endsWith(".xls"))
			{
				fileName=fileName+".xls";
			}
		 	response.reset();    
		    //response.setContentType("application/x-msdownload");    
		    try {
		    	String encodeName=java.net.URLEncoder.encode(fileName,"utf-8");
				response.setContentType("application/x-msdownload;charset=utf-8");
				response.setHeader("Content-Disposition", "attachment;filename="+encodeName);
				response.setHeader("expire", "0");
				response.setHeader("pragma", "no-cache");
				
				//response.setHeader("Content-Disposition","attachment; filename="+new String(fileName.getBytes("gb2312"),"ISO-8859-1"));
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}    
		    ServletOutputStream outStream=null;
			try {
				outStream = response.getOutputStream();
			} catch (IOException e1) {
				e1.printStackTrace();
			}    
		   
		    try{    
		        outStream = response.getOutputStream();    
		        workbook.write(outStream);    
		    }catch(Exception e)    
		    {    
		     e.printStackTrace();    
		    }finally{    
		        try {
					outStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}    
		    }   
	}
	public HSSFWorkbook getWorkbook()
	{
		return workbook;
	}
	public POIExcelHeadMap getHeadMap() {
		return headMap;
	}
	public List<Map<String, Object>> getDataMapList() {
		return dataMapList;
	}
	
}
