package org.myhibernate.excel;

import java.lang.reflect.Field;

public class ExcelXmlUtil 
{
	public static XML getXmlMap(Class cls)
	{
		XML xml=new XML("excel");
		xml.addAttribute("filename", "");
		
		XML idXml=new XML("id");
		idXml.addAttribute("title", "ID");
		idXml.addAttribute("column", "id");
		xml.addChild(idXml);
		
		Field[] fields= cls.getDeclaredFields();
		
		for (int i = 0; i < fields.length; i++) 
		{
			Field field=fields[i];
			XML node=new XML("column");
			node.addAttribute("title", "");
			node.addAttribute("column", field.getName());
			xml.addChild(node);
		}
		
		return xml;
	}
	public static void main(String[] args) {
		//Class cls=TcJxSalary.class;
		//System.out.println(getXmlMap(cls).toString());
	}
}
