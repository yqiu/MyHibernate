package org.myhibernate.excel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Demo {
	public static void main(String[] args) 
	{
		
		POIExcelHeadMap headMap=new POIExcelHeadMap();
		headMap.add("name", "姓名").add("age", "年龄").add("sex", "性别").add("salary", "薪资");
		
		List<Map<String,Object>> dataMapList=new ArrayList<Map<String,Object>>();
		Map<String, Object> dataMap=null;
		dataMap=new HashMap<String, Object>();
		dataMap.put("name", "张三");
		dataMap.put("age", "23");
		dataMap.put("sex", "男");
		dataMap.put("salary", "5555");
		dataMapList.add(dataMap);
		
		dataMap=new HashMap<String, Object>();
		dataMap.put("name", "李四");
		dataMap.put("age", "25");
		dataMap.put("sex", "男");
		dataMap.put("salary", "4323");
		dataMapList.add(dataMap);
		
		dataMap=new HashMap<String, Object>();
		dataMap.put("name", "王五");
		dataMap.put("age", "24");
		dataMap.put("sex", "女");
		dataMap.put("salary", "3232");
		dataMapList.add(dataMap);
		
		POIExcelExport excelExport=new POIExcelExport(headMap, dataMapList);
		excelExport.save("e:/test/Excel导出测试.xls");
		
		System.out.println("finish");
	}

}
