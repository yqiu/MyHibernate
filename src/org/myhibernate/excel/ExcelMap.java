package org.myhibernate.excel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExcelMap
{
	private String idTitle="";
	private String idField="";
	private boolean isExistId=false;
	private Map<String, String> map=new HashMap<String, String>();
	private List<String> keyList=new ArrayList<String>();
	private List<String> valueList=new ArrayList<String>();
	
	public String getIdField() {
		return idField;
	}
	public String getIdTitle() {
		return idTitle;
	}
	public Map<String, String> getMap() {
		return map;
	}
	public void setIdField(String idField) {
		isExistId=true;
		this.idField = idField; 
	}
	public void setIdTitle(String idTitle) {
		isExistId=true;
		this.idTitle = idTitle;
	}
	public void setMap(Map<String, String> map) 
	{
		this.map = map;
	}
	public void set(String key,String value)
	{
		if ((key==null)||(key.trim().length()==0)) {
			return ;
		}
		getKeyList().add(key);
		getValueList().add(value);
		getMap().put(key, value);
	}
	public List<String> getKeyList() {
		return keyList;
	}
	public List<String> getValueList() {
		return valueList;
	}
	@Override
	public String toString() {
		return getIdTitle()+"="+getIdField()+","+getMap();
	}
	public boolean hasId()
	{
		return isExistId;
	}
}
