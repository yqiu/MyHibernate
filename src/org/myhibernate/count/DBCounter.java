package org.myhibernate.count;

import org.myhibernate.cache.CacheContainer;
import org.myhibernate.cache.CacheContainers;

public class DBCounter 
{
	private String id="";
	private DBCount count=null;
	public DBCounter(String id) 
	{
		this.id = id;
		CacheContainer container=CacheContainers.getCache(DBCount.class);
		if (container.get(id)!=null) {
			count=(DBCount) container.get(id);
		}
		DBCountTimer.init(); 
	}
	public String getId() {
		return id;
	}
	public DBCount getCount() {
		return count;
	}
	public DBCount getCountInstance()
	{
		if (count == null)
		{
			CacheContainer container=CacheContainers.getCache(DBCount.class);
			count=new DBCount();
			count.setId(id);
			container.set(id,count);
		}
		return count;
	}
}
