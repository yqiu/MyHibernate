package org.myhibernate.util;

import java.util.Iterator;
import java.util.TreeMap;
import java.util.UUID;

public class XML 
{
	private String id;
	private String text;
	private String type;
	private TreeMap<String, XML> childsMap=new TreeMap<String, XML>();
	private TreeMap<String, String> attrisMap=new TreeMap<String, String>();
	public  static String HEAD="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	public XML() 
	{ 
		this.id=UUID.randomUUID().toString();
	}
	
	public XML(String type)
	{
		this();
		this.type = type;
	}

	public void addChild(XML xml)
	{
		childsMap.put(xml.getId(), xml);
	}
	public XML getChild(String id)
	{
		return childsMap.get(id);
	}
	public TreeMap<String, XML> getChilds()
	{
		return childsMap;
	}
	public void addAttribute(String name,String value)
	{
		attrisMap.put(name, value);
	}
	public String getAttribute(String name)
	{
		return attrisMap.get(name);
	}
	public TreeMap<String, String> getAttributes()
	{
		return attrisMap;
	}
	public String toString()
	{
		Iterator<String> iterator=null;
		String key="";
		StringBuffer buffer=new StringBuffer("<"+type);
		iterator=attrisMap.keySet().iterator();
		while (iterator.hasNext())
		{
			key=iterator.next();
			buffer.append(" "+key+"=\""+attrisMap.get(key)+"\"");
		}
		buffer.append(">");
		iterator=childsMap.keySet().iterator();
		while (iterator.hasNext())
		{
			buffer.append(childsMap.get(iterator.next())+"\n");
		}
		if (getText()!=null)
		{
			buffer.append(getText());
		}
		
		
		buffer.append("</"+type+">");
		return buffer.toString();
	}
	
	public String getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
