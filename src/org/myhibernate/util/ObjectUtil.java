package org.myhibernate.util;

import java.beans.PropertyDescriptor;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class ObjectUtil 
{
	public void mapKeyReplace(Map<String,Object> dataMap,Map<String,String> keyMap)
	{
		Iterator<String> iterator=keyMap.keySet().iterator();
		String key="";
		String keyValue="";
		while (iterator.hasNext())
		{ 
			key=iterator.next();
			keyValue=keyMap.get(key);
			try {
				key=URLDecoder.decode(URLDecoder.decode(key,"utf-8"),"utf-8");
			} catch (UnsupportedEncodingException e) {
				//e.printStackTrace();
			} 
			if (dataMap.get(key)!=null)
			{
				dataMap.put(keyValue, dataMap.get(key));
				dataMap.remove(key);
			}
		}
	}
	public void setObjectData(Object object,Map<String, Object> dataMap)
	{
		if (dataMap==null||object==null) {
			return ;
		}
		Class cls=object.getClass();
		Field[] fields=cls.getDeclaredFields();
		Field field=null;
		String fieldName="";
		Object dataValue=null;
		String methodName="";
		Method method =null;
		for (int i = 0; i < fields.length; i++) 
		{
			field=fields[i];
			fieldName=field.getName();
			dataValue=dataMap.get(fieldName);
			if (dataValue!=null)
			{
				try 
				{
					methodName="set"+(fieldName.charAt(0)+"").toUpperCase()+fieldName.substring(1);
					method = cls.getDeclaredMethod(methodName, field.getType());
					method.invoke(object,dataValue);
				}
				catch (Exception e)
				{
					System.out.println("Exception for : "+field.getName()+",methodName="+methodName+",dataValue="+dataValue);
					e.printStackTrace();
				}
			}
		}
	}
	public void setObjectData(Object object,HttpServletRequest request)
	{
		Map<String,Object> dataMap=new HashMap<String, Object>();
		Map<String,Object> paramMap=request.getParameterMap();
		Iterator<String> iterator=paramMap.keySet().iterator();
		String key="";
		while (iterator.hasNext()) 
		{
			key=iterator.next();
			try {
				dataMap.put(key, URLDecoder.decode(request.getParameter(key),"utf-8"));
			} catch (UnsupportedEncodingException e) {
				//e.printStackTrace();
			}
		}
		setObjectData(object, dataMap);
	}
	public Object toObject(Class cls,Map<String, Object> dataMap)
	{
		if (dataMap==null) {
			return null;
		}
		Object object=null;
		try {
			object=cls.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		setObjectData(object, dataMap);
		return object;
	}
	public Object toObject(Class cls,HttpServletRequest request,Map<String, Object> extendMap)
	{
		Object object=null;
		Map<String,Object> dataMap=new HashMap<String, Object>();
		Map<String,Object> paramMap=request.getParameterMap();
		Iterator<String> iterator=paramMap.keySet().iterator();
		String key="";
		while (iterator.hasNext()) 
		{
			key=iterator.next();
			dataMap.put(key, request.getParameter(key));
		}
		if (extendMap !=null)
		{
			iterator=extendMap.keySet().iterator();
			while (iterator.hasNext()) 
			{
				key=iterator.next();
				dataMap.put(key, extendMap.get(key));
			}
		}
		object=toObject(cls, dataMap);
		return object;
	}
	
	public Json toJson(Object object)
	{
		Json json=new Json();
		if (object==null) {
			return json;
		}
		Class cls=object.getClass();
		Field[] fields=cls.getDeclaredFields();
		Field field=null;
		String fieldName="";
		Object dataValue=null;
		PropertyDescriptor descriptor=null;
		 Method rM =null;// descriptor.getReadMethod();
		for (int i = 0; i < fields.length; i++) 
		{
			field=fields[i];
			fieldName=field.getName();
			try {
				 descriptor= new PropertyDescriptor(fieldName, cls);
				 rM = descriptor.getReadMethod();
				 dataValue=rM.invoke(object);
				 if (dataValue!=null) 
				 {
					//dataValue=handleList((List)dataValue);
					 dataValue=handleValue(dataValue);
					json.add(fieldName, dataValue);
				 }
				 
			} catch (Exception e) {
			}
			
		}
		return json;
	}
	public Json toJson(Object object,List<String> filterFields)
	{
		Json json=new Json();
		if (object==null) {
			return json;
		}
		Class cls=object.getClass();
		Field[] fields=cls.getDeclaredFields();
		Field field=null;
		String fieldName="";
		Object dataValue=null;
		PropertyDescriptor descriptor=null;
		 Method rM =null;// descriptor.getReadMethod();
		for (int i = 0; i < fields.length; i++) 
		{
			field=fields[i];
			fieldName=field.getName();
			try {
					if (filterFields.contains(fieldName)==false)
					{
						 descriptor= new PropertyDescriptor(fieldName, cls);
						 rM = descriptor.getReadMethod();
						 dataValue=rM.invoke(object);
						 json.add(fieldName, dataValue);
					}
				
			} catch (Exception e) {
			}
			
		}
		return json;
	}
	public String toJsonString(Object object)
	{
		if (object==null) {
			return "{}";
		}
		Class cls=object.getClass();
		StringBuffer buffer=new StringBuffer("{");
		Field[] fields=cls.getDeclaredFields();
		Field field=null;
		String fieldName="";
		Object dataValue=null;
		PropertyDescriptor descriptor=null;
		 Method rM =null;// descriptor.getReadMethod();
		for (int i = 0; i < fields.length; i++) 
		{
			field=fields[i];
			fieldName=field.getName();
			try {
				 descriptor= new PropertyDescriptor(fieldName, cls);
				 rM = descriptor.getReadMethod();
				 dataValue=rM.invoke(object);
				 if (0==i)
				 {
					 buffer.append("\""+fieldName+"\":\""+dataValue+"\"");
				} else {
					 buffer.append(",\""+fieldName+"\":\""+dataValue+"\"");
				}
			} catch (Exception e) {
			}
			
		}
		buffer.append("}");
		return buffer.toString();
	}
	public List<String> toJsonString(List<Object> list)
	{
		List<String> list2=new ArrayList<String>();
		for (int i = 0; i < list.size(); i++) 
		{
			list2.add(toJsonString(list.get(i)));
		}
		return list2;
	}
	public List<Json> toJsonWithList(List<Object> list)
	{
		List<Json> list2=new ArrayList<Json>();
		for (int i = 0; i < list.size(); i++) 
		{
			list2.add(toJson(list.get(i)));
		}
		return list2;
	}
	public List<Json> toJsonWithList(List<Object> list,List<String> filterFields)
	{
		List<Json> list2=new ArrayList<Json>();
		for (int i = 0; i < list.size(); i++) 
		{
			list2.add(toJson(list.get(i),filterFields));
		}
		return list2;
	}
	public String toHtmlTableString(Object object)
	{
		if (object==null) {
			return "";
		}
		Class cls=object.getClass();
		StringBuffer buffer=new StringBuffer("<tr>");
		Field[] fields=cls.getDeclaredFields();
		Field field=null;
		String fieldName="";
		Object dataValue=null;
		PropertyDescriptor descriptor=null;
		 Method rM =null;// descriptor.getReadMethod();
		for (int i = 0; i < fields.length; i++) 
		{
			field=fields[i];
			fieldName=field.getName();
			try {
				 descriptor= new PropertyDescriptor(fieldName, cls);
				 rM = descriptor.getReadMethod();
				 dataValue=rM.invoke(object);
				 if (dataValue==null) {
					dataValue="";
				}
				 buffer.append("<td>"+dataValue+"</td>");
			} catch (Exception e) {
			}
			
		}//endfor
		buffer.append("</tr>");
		return buffer.toString();
	}
	 public  Map<String,String> toMap(String strs,String seperator1,String seperator2)
	 {
		 Map<String,String> map=new HashMap<String, String>(); 
		 String[] keys=strs.split(seperator1);
		 String key="";
		 for (int i = 0; i < keys.length; i++) 
		 {
			 key=keys[i];
			 if ((key!=null)&&(key.trim().length()>2)&&(key.split(seperator2).length==2))
			 {
				 map.put(key.split(seperator2)[0].trim(), key.split(seperator2)[1]);
			 }
		}
		 return map;
	 }
	 public Map<String,Object> toMap(Object object)
	{
		 Map<String,Object> map=new HashMap<String, Object>();
		 
			if (object==null) {
				return map;
			}
			Class cls=object.getClass();
			Field[] fields=cls.getDeclaredFields();
			Field field=null;
			String fieldName="";
			Object dataValue=null;
			PropertyDescriptor descriptor=null;
			 Method rM =null;// descriptor.getReadMethod();
			for (int i = 0; i < fields.length; i++) 
			{
				field=fields[i];
				fieldName=field.getName();
				try {
					 descriptor= new PropertyDescriptor(fieldName, cls);
					 rM = descriptor.getReadMethod();
					 dataValue=rM.invoke(object);
					 map.put(fieldName, dataValue);
				} catch (Exception e) {
				}
				
			}
			return map;
		}
	 public Map<String,String> toMap(HttpServletRequest request)
	{
			 Map<String,String> map=new HashMap<String, String>();
			 Map<String,String> paramMap=request.getParameterMap();
			 Iterator<String> iterator=paramMap.keySet().iterator();
			 String name="";
			 while(iterator.hasNext())
			 {
				 name=iterator.next();
				 map.put(name,request.getParameter(name));
				 
			 }
			 return map;
	}
	public List handleList(List list)
	{
		List<String> temp=new ArrayList<String>();
		for (int j = 0; j < list.size(); j++) 
		{
			Object object=list.get(j);
			if (object instanceof String) 
			{
				temp.add("\""+list.get(j)+"\"");
			}
		}
		return temp;
	}
	public Object handleValue(Object value)
	{
		if (value instanceof List)
		{
			List list=(List) value;
			List<String> temp=new ArrayList<String>();
			for (int j = 0; j < list.size(); j++) 
			{
				Object object=list.get(j);
				if (object instanceof String) 
				{
					temp.add("\""+list.get(j)+"\"");
				}
			}
			return temp;
		}
		if (value instanceof Map)
		{
			StringBuffer buffer=new StringBuffer();
			Map tempMap=(Map) value;
			Iterator iterator=tempMap.keySet().iterator();
			Object key=null;
			while(iterator.hasNext())
			{
				key=iterator.next();
				buffer.append(key+"="+tempMap.get(key)+";");
			}
			return buffer.toString();
		}
		return value;
	}
}
