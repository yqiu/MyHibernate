package org.myhibernate.util;

import java.util.ArrayList;
import java.util.List;

public class Json 
{
	private List<String> key_list=new ArrayList();
	private List<Object> value_list=new ArrayList();
	
	public Json() { 
	}
	public void add(String key,Object o)
	{
		key_list.add(key);
		if (o instanceof String)
		{
			String temp=o+"";// %26 %25
			value_list.add(temp.replaceAll("\"", "%22").replaceAll("&", "%26"));
		} else {
			value_list.add(o);
		}
	} 
	public Object get(String key)
	{
		for (int i = 0; i < key_list.size(); i++)
		{
			if (key.equals(key_list.get(i))) {
				return value_list.get(i);
			}
		}
		return null;
	}
	public void set(String key,Object value)
	{
		for (int i = 0; i < key_list.size(); i++)
		{
			if (key.equals(key_list.get(i)))
			{
				value_list.set(i,value);
				return;
			}
		}
		key_list.add(key);
		value_list.add(value);
	}
	@Override
	public String toString() 
	{
		StringBuffer buffer=new StringBuffer("{");
		String key="";
		Object value=null;
		int count=0;
		for (int i = 0; i < key_list.size(); i++)
		{
			key=key_list.get(i)+"";
			value=value_list.get(i);
			if (value instanceof List)
			{
				value=handleList((List)value);
				
			}
			if (value instanceof String)
			{
				if (0==count)
				{
					buffer.append("\""+key+"\":\""+value+"\"");
				} else 
				{
					buffer.append(",\""+key+"\":\""+value+"\"");
				}
			}
			else
			{
				if (0==count)
				{
					buffer.append("\""+key+"\":"+value);
				} else 
				{
					buffer.append(",\""+key+"\":"+value);
				}
			}
			count++;
		}
		
		buffer.append("}");
		return buffer.toString(); 
	}
	private List handleList(List list)
	{
		if ((list == null) ||(list.size()==0)) {
			return list;
		}
		if (list.get(0) instanceof String)
		{
			List<String> tempList=new ArrayList<String>();
			for (int i = 0; i < list.size(); i++) 
			{
				if (list.get(i) instanceof String)
				{
					tempList.add("\""+list.get(i)+"\"");
				}
			}
			return tempList;
		}else {
			return list ;
		}
		
	}
	public static void main(String[] args) 
	{
		List<String> list=new ArrayList<String>();
		list.add("aa");
		list.add("bbb");
		
		Json json=new Json();
		json.add("data",list);
		System.out.println(json);
		
	}
	
}
